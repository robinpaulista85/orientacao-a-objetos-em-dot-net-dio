            Tipos por valor 

Todos os tipos de dados numéricos.

Nâo precisam ser iniciados com o operador new.

A variável armazena o valor diretamente. 

A atribuição de uma variável a outra copia o conteúdo, criando efetivamente outra cópia da variável. 

Não podem conter o valor null.

Exemplos: integers, doubles, floats e char. 

                    Inteiros 

Os tipos inteiros (integers) têm sempre o mesmo significado, independente da implementação. 

                Double e Float 

Os números de ponto flutuante são bastante convencionais, as operações de ponto flutuante não geram erros (double, float).

                    Caracteres

Em C#, todos os caracteres (char) são armazenados no padrão Unicode e usam 16 bits por caractere.O Unicode permite armazenar os caracteres de todas as línguas vivas e algumas mortas como o sânscrito. 

                Tipos por referência

Um tipo de referência armazena uma referencia a seus dados. os tipos de referência incluem o seguinte:

- Duas variáveis podem conter a referência a um mesmo objeto;
- Operações em uma afetarem a outra.

Todas as matrizes, mesmo que seus elementos sejam do tipo de valor.
Exemplos de tipos por referência: Strings, classes e arrays.

                    String

Semelhante ao char, strings são variáveis do tipo texto.
São uma sequência de caracteres, geralmente utilizada para representar palavras, frases ou textos de um programa. 

As strings são consideradas imutáveis e não podem ser alteradas depois de criadas. Quando você efetua uma operação qualquer, como por exemplo, concatenar um caractere, você na verdade está criando outra string e descartando a anterior. 

                    Arrays 

Um array (matriz) é uma lista de valores onde todos os valores no grupo são referenciados pelo nome da matriz e o índice atribuído ao valor específico na matriz. 

                    Ponteiro 

Um ponteiro ou apontador é um tipo de dado de cujo valor se refere diretamente a um outro valor alocado em outra área da memória, através de seu endereço. 