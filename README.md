# orientacao-a-objetos-em-dot-net-dio

- Nesse curso o tema proposto foi voltado para iniciantes em C# e .NET, que foca no aprendizado teórico dos principais 
conceitos em orientação a objetos, como métodos, classes e propriedades. 

Expert da DIO: Instrutor Bruno Dias (https://www.linkedin.com/in/brunodecamposdias/)

Curso presente na plataforma da **Digital Innovation One** no link https://web.digitalinnovation.one/course/orientacao-a-objetos-em-net.
